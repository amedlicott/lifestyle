package com.digitalml;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;


public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        port(4567);

        staticFiles.location("/public");
        redirect.get("", "/");
        redirect.get("/", "/welcome.html");

        after((request, response) -> {
            response.header("Content-Encoding", "gzip");
        });


        get("/lifestyle/party/:id", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.get("http://ignite-runtime.com/party/10/party/:id").routeParam("id", req.params("id")).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());

            return res;
        });

        post("/lifestyle/party", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.post("http://ignite-runtime.com/party/10/party").body(req.body()).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());

            return res;
        });

        get("/lifestyle/policy/:id", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.get("http://ignite-runtime.com/policy/12/policy/:id").routeParam("id", req.params("id")).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());
            return res;
        });

        get("/lifestyle/user/-/activities.json", (req, res) -> {
            // Authenticate
            HttpResponse<String> auth = Unirest.post("http://fitbit.com/api/1/user/logon").queryString("username", "andymed").queryString("password", "nottelling").asString();
            if (auth.getStatus() == 302) {
                res.status(auth.getStatus());
                return res;
            }

            HttpResponse<String> proxyResponse = Unirest.get("http://fitbit.com/api/1/user/-/activities.json").routeParam("id", req.params("id")).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());
            return res;
        });

        patch("/lifestyle/policy", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.patch("http://ignite-runtime.com/party/12/policy").body(req.body()).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());

            return res;
        });

        patch("/lifestyle/party", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.patch("http://ignite-runtime.com/party/10/party").body(req.body()).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());

            return res;
        });

        post("/lifestyle/payment", (req, res) -> {
            HttpResponse<String> proxyResponse = Unirest.post("http://ignite-runtime.com/party/15/payment").body(req.body()).asString();
            res.body(proxyResponse.getBody());
            res.status(proxyResponse.getStatus());

            return res;
        });
    }
}
